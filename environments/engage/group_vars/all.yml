---

debug: false

# common variables
aptcachetime: 3600
locale: "en_US.UTF-8"
localuser: "{{ lookup('env','USER') }}"
localhome: "{{ lookup('env','HOME') }}"

# containerd
containerd_version: 1.4.3-1
activate_containerd: true
crictl_version: v1.18.0
activate_nvidia: false
ignore_nvidia_fail: false
containerd_ubuntu_version: xenial

log_docker: true
log_kubernetes: true

kube_state_metrics: false

swapfile_path: /swapfile
swapfile_size: 100
swapfile_fallocate: true

xilinx_xrt_install: false

# 470 is the last driver branch to support the Kepler architecture,
# which GPUs in PSI Low are based on
# https://docs.nvidia.com/datacenter/tesla/tesla-release-notes-510-47-03/index.html
nvidia_driver_ubuntu_branch: "470"

http_proxy: 
https_proxy: 
no_proxy: localhost,127.0.0.1,10.96.0.0/12,192.168.0.0/16,202.9.15.0/24,172.17.0.1/16

proxy_env:
  http_proxy: "{{ http_proxy }}"
  https_proxy: "{{ https_proxy }}"
  no_proxy: "{{ no_proxy }}"

docker_hub_mirror: ~
central_nexus_mirror: "http://{{ hostvars['psi-mid-head'].ansible_host }}:9082"
gitlab_nexus_mirror: "http://{{ hostvars['psi-mid-head'].ansible_host }}:9082"

# These are duplicated from elasticstack_vars.yml because they're used
# in the `logging` role, which is executed as part of the standard node
# setup. They should really appear in that role's defaults.
filebeat_image: docker.elastic.co/beats/filebeat:7.17.2

pod_network_ipip_mode: CrossSubnet



##### Ceph #####

install_ceph_prerequisites: false

# ceph vars
cephadm_ceph_release: quincy
cephadm_ceph_releases:
  - "quincy"

# Known problem with sqlite - so need a specific image
# https://www.spinics.net/lists/ceph-users/msg73038.html
cephadm_image: "quay.ceph.io/ceph-ci/ceph:{{ cephadm_ceph_release }}"
cephadm_enable_dashboard: true
cephadm_enable_firewalld: false
cephadm_enable_monitoring: false
cephadm_bootstrap_additional_parameters: ""
cephadm_apt_repo_dist: "focal"
cephadm_public_interface: "ens3"
cephadm_public_network: "192.168.100.0/24"

ska_ceph_safe_delete: false
 
cephadm_osd_spec: |
  service_type: osd
  service_id: osd_using_paths
  placement:
    host_pattern: '*'
  spec:
    data_devices:
      paths:
      - /dev/vdb

cephadm_pools:
  - name: cephfs_data
    size: 2
    pg_num: 8
    pgp_num: 8
  - name: cephfs_metadata
    size: 2
    pg_num: 8
    pgp_num: 8

  - name: volumes
    size: 2
    pg_num: 8
    pgp_num: 8
    application: rbd

cephadm_bootstrap_configs:
  - "set mon mon_data_avail_warn 10"
  - "set mon mon_allow_pool_delete True"
  - "set global osd_pool_default_min_size 1"

cephadm_post_commands:
  - "fs new cephfs cephfs_metadata cephfs_data"
  - "orch apply mds cephfs"