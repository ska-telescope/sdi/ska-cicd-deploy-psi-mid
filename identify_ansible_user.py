#!/usr/bin/env python3

"""
Prints the first user from base_node_vars.yml that matches a public key
in ~/.ssh/. Public keys and users are checked in arbitrary order.

Requires that PyYAML is present in the Python environment, which it
should be assuming it's the environment that Ansible is installed in.
"""

import glob
import os.path
import sys
import yaml

with open("base_node_vars.yml") as f:
    users = yaml.safe_load(f)['add_ssh_keys_admin']

for fn in glob.glob(os.path.expanduser("~/.ssh/*.pub")):
    with open(fn) as f:
        contents = f.read().strip()
    for user, ssh_keys in users.items():
        if isinstance(ssh_keys, dict):
            ssh_keys = ssh_keys['ssh_keys']
        if contents in ssh_keys:
            print(user)
            sys.exit()
else:
    sys.exit(1)
