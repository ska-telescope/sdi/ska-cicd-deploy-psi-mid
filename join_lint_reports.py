import xml.etree.ElementTree as ET
import glob

# Linting files
lintFiles = glob.glob("linting-*.xml")

# Linting Results
tests = []
for file in lintFiles:
    tree = ET.parse(file)
    root = tree.getroot()

    for child in root:
        if child.tag == "testsuite":
            tests.append(child)


# Export Joint XML
newRoot = ET.Element("testsuites")

for test in tests:
    newRoot.append(test)

tree = ET.ElementTree(newRoot)
tree.write("build/reports/linting.xml")