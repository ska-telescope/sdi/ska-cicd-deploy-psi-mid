# README

Deploy PSI Mid Resources

## Summary

This repo builds the PSI Mid management node and worker nodes with Prometheus, Ceph, Rook, ElasticStack and Kubernetes

Install the Ansible collections:
```
$ make install
```
(there is also the `make reinstall` target that could be used to update the current versions of the collections)

## Deploying Ceph

```
$ make build_ceph
```
This will start by destroying any existing Ceph cluster in the targeted nodes (*environments/psi-mid/hosts*) and, only then, deploying a new Ceph cluster based on the [stackhpc/cephadm ansible collection](https://github.com/stackhpc/ansible-collection-cephadm).

There are some changes that should be put in place before running the ceph deployment make target:
- The *ssh.config*_* file should be updated according to the specific ST member's required ssh config to connect to PSI Mid
- The *ansible_user* variable in the *environments/psi-mid/hosts* file should be updated according to the specific ST member's PSI Mid SSH user
- In the *collections/ansible_collections/stackhpc/cephadm/roles/cephadm/templates/cluster.yml.j2* template, the **hostname: {{ hostvars[host].ansible_facts._hostname_ }}** line should be updated to **hostname: {{ hostvars[host].ansible_facts._fqdn_ }}**
- In the *collections/ansible_collections/stackhpc/cephadm/roles/cephadm/tasks/pkg_debian.yml* file, the **keyserver: _keyserver.ubuntu.com_** line - under the *Add Ceph signing keys* task, should be updated to **keyserver: _hkp://keyserver.ubuntu.com:80_**

## Deploying Ceph

Make sure you have the collection version downloaded. Metallb is in the ska_cicd-k8s collection.

```
$ make build_ceph
```
