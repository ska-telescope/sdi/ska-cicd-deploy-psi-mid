NODE_NAME ?= psi-mid
INVENTORY_FILE ?= ./inventory_psi_mid
BASE_INVENTORY_FILE ?= ./environments/psi-mid
CEPH_NODE_NAME ?= ceph
CEPH_INVENTORY_FILE ?= $(INVENTORY_FILE)
CEPH_VARS ?= ./cephcluster_vars.yml
NODES ?= all
BASE_VARS ?= ./base_node_vars.yml
CEPH_NODE_VARS ?= ./$(CEPH_NODE_NAME)_vars.yml
DEBUG ?= false
TAGS ?= all
EXTRA_VARS ?=
COLLECTIONS_PATH ?= collections
ROLES_PATH ?= roles
PODMAN_REGISTRY_MIRROR ?= docker.io
REGISTRY_MIRROR ?= https://$(PODMAN_REGISTRY_MIRROR)
V ?=
THIS_BASE=$(shell pwd)

# try to match a public key from ~/.ssh with a public key from base_user_vars.yml
# export ANSIBLE_REMOTE_USER ?= $(shell ./identify_ansible_user.py)

.DEFAULT_GOAL := help

.PHONY:
# define overides for above variables in here
-include PrivateRules.mak

STACK_CLUSTER_PLAYBOOKS := $(COLLECTIONS_PATH)/ansible_collections/ska_cicd/stack_cluster/playbooks
DOCKER_PLAYBOOKS := $(COLLECTIONS_PATH)/ansible_collections/ska_cicd/docker_base/playbooks
K8S_PLAYBOOKS := $(COLLECTIONS_PATH)/ansible_collections/ska_cicd/k8s/playbooks

reinstall: uninstall install ## reinstall collections

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "NODE_NAME=$(NODE_NAME)"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE)"
	@echo "EXTRA_VARS=$(EXTRA_VARS)"
	@echo "STACK_CLUSTER_PLAYBOOKS=$(STACK_CLUSTER_PLAYBOOKS)"
	@echo "DOCKER_PLAYBOOKS=$(DOCKER_PLAYBOOKS)"
	@echo "K8S_PLAYBOOKS=$(K8S_PLAYBOOKS)"
	@echo "DEBUG=$(DEBUG)"

all: build

sub:  ## update git submodules
	git submodule init
	git submodule update --recursive --remote
	git submodule update --init --recursive

uninstall:  # uninstall collections
	rm -rf $(COLLECTIONS_PATH)/ansible_collections
	rm -rf $(ROLES_PATH)

$(COLLECTIONS_PATH)/ansible_collections:
	ansible-galaxy install -r requirements.yml

install: $(COLLECTIONS_PATH)/ansible_collections  ## Install dependent ansible collections

reinstall: uninstall install ## reinstall collections

build_common:  ## apply the common roles
	ansible-playbook  -i $(BASE_INVENTORY_FILE) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(BASE_VARS) $(V)

check: ## Health check
	ip=`ansible -i $(INVENTORY_FILE) cluster -m debug -a "var=ansible_host" | grep -oP '\d+(\.\d+){3}' | head -1`; \
	curl "http://$${ip}:9200/_cluster/health/?wait_for_status=yellow&timeout=50s&pretty"

nodecheck: ## Node state
	ip=`ansible -i $(INVENTORY_FILE) cluster -m debug -a "var=ansible_host" | grep -oP '\d+(\.\d+){3}' | head -1`; \
	curl "http://$${ip}:9200/_nodes/process?pretty"

statecheck: ## Whole cluster state
	ip=`ansible -i $(INVENTORY_FILE) cluster -m debug -a "var=ansible_host" | grep -oP '\d+(\.\d+){3}' | head -1`; \
	curl "http://$${ip}:9200/_cluster/state?pretty"

# custom version of haproxy config because proxy on same as head node
build_haproxy: ## Build haproxy
	ansible-playbook -i $(INVENTORY_FILE) $(K8S_PLAYBOOKS)/loadbalancers.yml \
	  -e @$(BASE_VARS) $(V)

build_k8s: ## Build k8s
	ansible-playbook -i $(INVENTORY_FILE) $(K8S_PLAYBOOKS)/setup.yml \
	-e @$(BASE_VARS) \
	--extra-vars="debug=$(DEBUG)" $(V)

build_charts: ## Build charts
	ansible-playbook -i $(INVENTORY_FILE) $(K8S_PLAYBOOKS)/charts.yml \
	-e @$(BASE_VARS) \
	--extra-vars="debug=$(DEBUG)" $(V)

build_ceph:  ## apply the docker roles
	ansible-playbook  -i $(BASE_INVENTORY_FILE) playbooks/ceph.yml

build_metallb:  ## apply the docker roles
	ansible-playbook  -i $(BASE_INVENTORY_FILE) playbooks/metallb.yml

rook: ## Install Rook Ceph support
	cd rook && make build_rook

lint: install ## Lint check playbooks and roles
	yamllint -d "{extends: relaxed, rules: {line-length: {max: 256}}}" \
			-f parsable \
			playbooks/roles/* \
			playbooks/*.yml \
			| yamllint-junit -o linting-yamllint.xml;
	ANSIBLE_COLLECTIONS_PATHS=$(COLLECTIONS_PATH) \
	ansible-lint --nocolor  playbooks/roles/* \playbooks/*.yml -p | tee ansible-lint.txt;
	ansible-lint-junit ansible-lint.txt -o linting-ansible.xml
	flake8 --format junit-xml playbooks/roles/* --output-file linting-flake.xml

# Join different linting reports into linting.xml
# Zero, create linting.xml with empty testsuites
# First, delete newlines from the files for easier parsing
# Second, parse <testsuite> tags in <testsuites> in each file (disregard any attributes in testsuites tag)
# Final, append <testsuite> tags into linting.xml
join-lint-reports:
	@echo "<testsuites>\n</testsuites>" > build/reports/linting.xml; \
	for FILE in linting-*.xml; do \
	TEST_RESULTS=$$(tr -d "\n" < $${FILE} | \
	sed -e "s/.*<testsuites[^<]*\(.*\)<\/testsuites>.*/\1/"); \
	TT=$$(echo $${TEST_RESULTS} | sed 's/\//\\\//g'); \
	sed -i.x -e "/<\/testsuites>/ s/.*/$${TT}\n&/" build/reports/linting.xml; \
	rm -f build/reports/linting.xml.x; \
	done
.PHONY: join-lint-reports

join-lint-reports-python-script:
	python3 join_lint_reports.py


help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

